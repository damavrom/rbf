# Radial Based Function Networks: an experiment on two algorithms

#### Description

This repository contains code that implements two algorithms that set
the RBF neurons. These algorithms are the k-means clustering algorithm
and a simple algorithm that I came up with that chooses the basis
function centers and weights values in a supervised manner.

This repo displays my approach on setting RBF parameters; the remarks
stated, the experiments performed and the chosen arguments passed to the
algorithms are according to my own consideration. In other words, this
repo is my personal academic project.

#### Contents
- [k-Means]
    - [Supervised k-Means]
    - [Implementation]
- [My Algorithm]
    - [Clarifications]
    - [Features]
- [Experiments]
    - [Technical Details]
    - [Dataset]
    - [Results]
    - [Secondary Experiments]
    - [Remarks]
- [Closure]

## k-Means

This is the textbook algorithm for setting the basis function centers.
The algorithm is essentially two steps that loop; choosing the means
-centers- of the already defined $`k`$ clusters and then defining the
clusters anew according to these centers. Each cluster is defined by the
elements that are closer to each center. This loop starts with choosing
the $`k`$ centers randomly and ends when the centers of the next loop
are the same as the time being. This algorithm is flawed at some
occasions but there have been made significant attempts to improve it, I
won't get into further details though. The centers can be set as simply
as that; running this algorithm with the whole training dataset as input
and the desired number of prototypes as $`k`$.

As I have chosen the textbook way of setting the centers, I have
accordingly chosen the textbook way of setting the beta values,
appropriately described by the formula:
```math
\beta = \frac{1}{2\sigma^2}
```
where $`\sigma`$ is the standard deviation of the cluster that each
center is the mean of.

#### Supervised k-Means

Using this algorithm once, with argument the whole training dataset is
the unsupervised approach; the label of each training element is not
considered at all and it doesn't make a difference to the algorithm. But
using the knowledge of the labels is an advantage that I would like to
use. So I separated the dataset according to the class each element
belongs to form as many datasets as classes there exist. Then I run the
k-means algorithm once for each of these datasets. Each time the
algorithm gives $`k`$ centers so in the end I have $`k \cdot n`$
prototypes where $`n`$ is the number of classes. The beta values are
chosen in the same way as the unsupervised k-means.

#### Implementation

The code in this repo implements this _menta_-algorithm. But the $`k`$
value is not the same on each run, it changes according to the shape of
the separated datasets.

## My Algorithm

This simple supervised clustering algorithm is based on interclass
distances.
The algorithm consists of a few steps:
1. It first calculates the minimum distance $`d_i`$ between any training
element $`i`$ and all the other elements that belong to classes other
than $`i`$ element's class.
2. Then for each class it constructs a queue of all its elements sorted
by their $`d_i`$ value. The rest of steps are done for all the acquired
queues.
3. It chooses the first element of the queue as a basis function center
and calculates its beta value according to the $`d_i`$ value.
4. It removes all elements of the queue that fall close enough to the
prototype chosen in the previous step.
5. It repeat's step 3 and 4 until queue is empty. The already defined
centers and beta values from step 3 are returned.

The basic notion of the algorithm is that instead of choosing all the
training elements as centers, I choose some and omit the ones that are
located very close to the ones I have chosen in order to reduce
redundant RBF neurons.

> **Note:** In this document and throughout the repository I refer to
this algorithm as _my algorithm_ or the _border algorithm_. This doesn't
mean this algorithm is rightfully mine. I must admit I did a poor study
on the subject and I don't know much about the respective literature; I
just know what I have been taught in the university as a post graduate
student. I don't want to claim mine or rename something that other
scientists have already established. It is true that this algorithm is
fruit of my imagination but I don't deny it can also be somebody's else.
It is a simple algorithm after all. Either way this algorithm satisfies
the purpose of this repository.

#### Clarifications

The way I presented this algorithm up to this point is very general so
there is need to consider some minor, but essential matters. These
matters are: the way the queues are sorted; ascending or descending, how
beta values are calculated and how _falling close enough to the
center_ is defined. For sorting, I intuitively have chosen to sort
the queues from the maximum $`d_i`$ to the minimum. I did so having
in mind that the most distant element from the other classes must have
the most elements of the queue falling closer to it, resulting to less
elements in the queue for the next iteration. Though this is heuristic
reasoning and can't guarantee better performance. For computing beta
values and defining what one element being close to the other means, I
came up with two values that I pass as a parameter to this algorithm;
$`c_\text{outer}`$ and $`c_\text{inner}`$. These values concern two
requirements I have regarding the RBF output.

The first requirement that I have is that the RBF output for an $`i`$
training element is less than or equal to $`c_\text{outer}`$ for all
centers that don't belong in the same class with $`i`$. So for each
$`c`$ center I set the associated beta value so that all elements that
don't belong to its class have RBF output less than or equal to
$`c_\text{outer}`$. The RBF is the Gaussian function;

```math
\phi(i) = e^{-\beta\|i-c\|^2}
```

The distance between $`c`$ and the closest of all elements that belong
to class of $`c`$ is the $`d_c`$ that we acquired in the first step of
the algorithm. If I define RBF to be equal to $`c_\text{outer}`$ when
$`i`$ is the closest element that doesn't belong to the class of $`c`$
then I have:

```math
c_\text{outer} = e^{-\beta d_c^2}
```

From this equation I get the beta value;

```math
\beta = - \frac{\log{c_\text{outer}}}{d_c^2}
```

The second requirement is that the RBF output for an $`i`$ training
element is more than or equal to $`c_\text{inner}`$ for at least one
center $`c`$ that belongs to the class of $`i`$. If the RBF output of
$`i`$ is more than $`c_\text{inner}`$ then it is consider to be very
close to $`c`$ and therefore it is removed from the queue. If on the
other hand $`i`$ is not close enough to $`c`$, it is left to be
investigated if it is suitable to be a center.

#### Features

- The algorithm is supervised because it takes into account the classes
of the training elements.
- The number of RBF neurons is dynamic. It depends on the shape of the
classes and how one class associates with the other; when the elements
of one class are tangled with the rest elements, then it is expected
that this algorithm will return many neurons. In contrast, in the ideal
scenario that all classes are clearly separated and of spherical shape,
the algorithm will return as many neurons as classes exist. In a
realistic scenario, the number of neurons returned depends on
$`c_\text{outer}`$, $`c_\text{inner}`$ and the size of dataset input.
- The complexity of this algorithm is very big; the computational burden
of calculating all possible distances makes it very slow and at times
forbidding. The simplest way to overcome this problem is to execute the
algorithm given only a sample of the dataset.

## Experiments

In order to test and compare the performance -accuracy- of these
algorithms I had to implement them and run them using a specific
dataset. For both algorithms we can presume that the more RBF neurons
indicate better accuracy but also bigger computational load. While this
becomes obvious in the results of the experiments, my main purpose with
these experiments was to examine how the accuracy correlates with the
number of RBF neurons that each algorithm returns, this number is
proportional to the computational load. Thus I ran the experiments
several times with parameters that resulted to various amounts of RBF
neurons to get a clear insight on this computational load - accuracy
ratio.

#### Technical Details

- The programming language I use is [Python 3] throughout the
repository.
- The neural network framework I used to implement the deep learning
part of the RBFNs is [PyTorch].
- I didn't implement k-means myself; I used the implementation in the
[scikit-learn] package.
- For facilitating the computations on matrices, I used the [NumPy]
package.
- For plotting the data I used [gnuplot]. The [plotting script] is
present in this repo.
- I don't own a GPU with an Nvidia chipset so I didn't bother
configuring anything to run on GPU.
- For testing the algorithms, I used almost the same architecture for
both cases. Technically, the only detail that is different is the number
of the RBF neurons that cannot be predetermined. All the other
parameters are the same; the architecture of the rest of the network,
the learning rate, the optimizer and so on. These parameters are:
  - batch size: 2
  - optimizer: gradient descent
  - learning rate: 0.5
  - hidden layers: the RBF layer and one with $`2^8`$ perceptrons.
  - number of epochs: 30
- The relevant parameters of my algorithm are:
  - $`c_\text{inner} = c_\text{outer} = 0.5`$
- The referred accuracy is the accuracy of the model in the test data.
It is not the accuracy after the last training epoch, but the greatest
among them all. It is more possible that this accuracy is after one of
last few epochs.

#### Dataset

The dataset I used for the experiments is the MNIST [database of
handwritten digits]. A go-to dataset for machine learning newbies. It is
a vast dataset already separated to training and testing datasets. It
has 60,000 training elements and 10,000 testing elements. The attributes
of its elements constitute a 2D greyscale image of 28x28 pixels. All
elements are labeled with a digit -integer- that the image displays.
Therefore these attributes are very intuitive and make dataset ideal for
understanding concepts of machine learning and artificial intelligence.
The obstacle that this dataset underlies is the numerous dimensions of
its data.

#### Results

I ran each algorithm 50 times. The results are well visualised by the
following number of neurons - accuracy plot.

![primary plot]

The diagram displays well how the accuracy of my algorithm remains above
the accuracy of k-means for almost any number of neurons. My algorithm
has the best results and k-means has the worst but still it is not
guaranteed for every run that my algorithm's accuracy will surpass
k-means'.

The results are presented sorted in these files: [k-means-primary.log],
[border-primary.log].

#### Secondary Experiments

I ran -I will run- more experiments with various parameters just for the
fun of it and not with intention to prove anything. In the following
cases I ran each experiment less times as they are not so important yet
they are time consuming. I also made use the data the preceding
experiment gave me.

In the following I compare my algorithm while use different
configurations for the $`c`$ parameters. In the case where
$`c_\text{inner}`$ is greater than $`c_\text{outer}`$ I set
$`c_\text{inner}=0.55`$ and $`c_\text{outer}=0.5`$, in the other case
$`c_\text{inner}=0.5`$ and $`c_\text{outer}=0.55`$.

![c plot]

In the following I run my algorithm normally along with an another
variation. In this variation, every RBF center that doesn't have another
training element close to it is dismissed. There is a minimum size of
a cluster that this center defines. This minimum is 2.

![cluster size plot]

In the following I compare the vanilla k-means with the supervised
k-means.

![k-means plot]

In the following I compare how computing distance effects the result. I
compare the Euclidean distance with Manhattan distance on my algorithm.

![distance-1 plot]

In the following I compare the Euclidean distance with a bizarre metric
that seems to have interesting results. This distance is
$`d(\mathbf{p}, \mathbf{q}) = (p_1-q_1)^2+(p_2-q_2)^2+\dots+(p_n-q_n)^2`$;
it is essentially the Euclidean distance but not squared.

![distance-2 plot]

Finally, in the following I compare the Euclidean distance with the
standardized Euclidean distance.

![distance-3 plot]

#### Remarks

These algorithms are not appropriate to be solely used on this specific
dataset as I did, at least the way they are implemented. Distance is
fundamental for both of these algorithms and in this implementation I
use euclidean distance that it tends to not work on spaces with numerous
dimensions. For better results this issue must be resolved either by
reducing the number of dimensions or/and by using another metric for
distance. Hence the metrics of these algorithm could be a hint about a
sequel of this project.

## Closure

In the end my personal goal with this repository, is not presenting a
couple of algorithms along with their properties and executing
experiments for their own sake. When I was taught about RBFNs, the few
ways of setting an RBFN presented in my school or in most academic or
non-academic websites -including Wikipedia- were choosing the RBF
centers to be the whole dataset, to be a random sample or using k-means.
This was how I have implemented my first RBFN; to use k-means for
setting the RBF centers. The results were so bad that discouraged me to
consider RBFNs as decent deep learning model, a grave mistake from the
part of a young scientist. So with this repository, I object to teaching
about RBFNs while using so unsettling implementations while I present a
simple and slightly better implementation.

If you found mistakes in any part of this work, or have something to
add, please don't hesitate to submit merge requests or contact me on my
e-mail. Also feel free to ask me for any clarifications you may need.
Thank you for reading!

[k-Means]: #k-means
[Supervised k-Means]: #supervised-k-means
[Implementation]: #implementation
[My Algorithm]: #my-algorithm
[Clarifications]: #clarifications
[Features]: #features
[Experiments]: #experiments
[Technical Details]: #technical-details
[Dataset]: #dataset
[Results]: #results
[Secondary Experiments]: #secondary-experiments
[Remarks]: #remarks
[Closure]: #closure

[primary plot]: experiments/primary.png
[c plot]: experiments/c.png
[k-means plot]: experiments/k-means.png
[cluster size plot]: experiments/cluster_size.png
[distance-1 plot]: experiments/distance1.png
[distance-2 plot]: experiments/distance2.png
[distance-3 plot]: experiments/distance3.png

[k-means-primary.log]: experiments/k-means-primary.log
[border-primary.log]: experiments/border-primary.log

[plotting script]: experiments/plot.p

<!-- References -->
[Python 3]: https://www.python.org/
[PyTorch]: https://pytorch.org/
[scikit-learn]: https://scikit-learn.org/
[NumPy]: https://numpy.org/
[gnuplot]: http://www.gnuplot.info/
[database of handwritten digits]: http://yann.lecun.com/exdb/mnist/
