import torch
import torch.nn as nn

import metrics

class MLP(nn.Module):
    def __init__(self, hidden_neurons):
        super(MLP, self).__init__()
        self.hiddenlayer = nn.Linear(28*28,  hidden_neurons)
        self.outputlayer = nn.Linear(hidden_neurons,  10)
        self.epoch = 0
    def forward(self, x):
        x = x.view(-1, 28*28)
        x = self.hiddenlayer(x)
        x = torch.sigmoid(x)
        x = self.outputlayer(x)
        x = torch.sigmoid(x)
        return x

class RBFN(nn.Module):
    def __init__(self, centers, betas, hidden_p):
        super(RBFN, self).__init__()
        self.centers = centers
        self.betas = betas
        self.epoch = 0
        self.hiddenlayer = nn.Linear(len(centers),  hidden_p)
        self.outputlayer = nn.Linear(hidden_p,  10)
        #self.outputlayer = nn.Linear(len(centers),  10)

    def forward(self, x):
        x = self.hiddenlayer(x)
        x = torch.sigmoid(x)
        x = self.outputlayer(x)
        x = torch.sigmoid(x)
        return x

    def rbf(self, x):
        x = x.reshape(-1, 28*28).float()
        return torch.cat([
            metrics.distance(elem, self.centers).pow(2).mul(-self.betas).exp()
            for elem in x
        ]).reshape(-1, len(self.centers))

def train(model, train_loader, optimizer):
    model.train()
    criterion = nn.MSELoss()
    for batch_idx, (data, target) in enumerate(train_loader):
        optimizer.zero_grad()
        output = model(data)
        target2 = torch.zeros(output.size())
        for idx, t in enumerate(target2):
            t[target[idx]] = 1
        loss = criterion(output, target2)
        loss.backward()
        optimizer.step()
        if batch_idx % 100 == 0:
            print('Training {}/{}\r'.format(
                batch_idx * len(data),
                len(train_loader.dataset)
            ), end='')
    model.epoch += 1

def test(model, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            output = model(data)
            pred = output.argmax(dim = 1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()

    print('{:.2f}%                      '.format(100.*correct/len(test_loader.dataset)))
    #f = open("RBFaccuracy", "a")
    #f.write(str(100 - 100. * correct / len(test_loader.dataset))+'\n')
    #f.close()
    return 100.*correct/len(test_loader.dataset)

