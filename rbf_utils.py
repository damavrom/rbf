import torch
import numpy
from numpy import linalg
from sklearn.cluster import KMeans

import metrics

def border_features(dataset, sample = 100, c_inner = 0.5, c_outter = 0.5, min_cluster_size = 1):
    train_loader = torch.utils.data.DataLoader(
        list(zip(dataset.data.float(), dataset.targets)),
        batch_size = sample,
        shuffle = True
    )
    train_set_array, train_set_targets = next(iter(train_loader))
    train_set_array = train_set_array.reshape((-1, 28*28))

    classified = {}
    for l in range(10):
        classified[l] = torch.cat([
            train_set_array[idx]
            for idx, label in enumerate(train_set_targets)
            if label == l
        ]).reshape(-1, 28*28)
    c_outter_log = numpy.log(c_outter)
    centers = []
    betas = []
    clusters = {}
    cnt = 1
    for l1 in classified:
        left = []
        for el in classified[l1]:
            print("{}/{}  \r".format(cnt, train_set_array.shape[0]), end = "")
            cnt += 1
            dists = []

            dists = torch.cat([
                        metrics.distance(el, classified[l2])
            for l2 in range(10)
            if l1 != l2 and classified[l2].shape[0] != 0
            ])
            left.append((el, dists.sort().values[:3].sum()/3))
            #left.append((el, dists.min()))
            #print(sum(dists[:3])/3, dists[0], sum(dists)/len(dists))
        cnt1 = 0
        while len(left) > 0:
            max_el, m = left[0]
            for el, min_dist in left:
                if min_dist > m:
                    m = min_dist
                    max_el = el
            beta = -c_outter_log/(m**2)
            new_left = []
            cnt2 = 0
            for el, min_dist in left:
                rbf = metrics.distance(el, max_el).pow(2).mul(-beta).exp()
                if rbf < c_inner:
                    new_left.append((el, min_dist))
                else:
                    cnt2 += 1
            left = new_left
            if cnt2 >= min_cluster_size:
                centers.append(max_el)
                betas.append(beta)
                cnt1 += 1
        print(l1, "total clusters:", cnt1)
        clusters[l1] = cnt1

    betas = torch.Tensor(betas)
    centers = torch.cat(centers).reshape((-1, 28*28))
    return centers, betas

def kmeans_features(dataset, max_clusters = 100, min_clusters = 75):
    train_set_array = dataset.data.numpy().reshape((-1, 28*28))
    train_set_targets = dataset.targets.numpy()

    print("separating dataset...")
    classified = {}
    for l in range(10):
        classified[l] = numpy.array([
            train_set_array[idx]
            for idx, label in enumerate(train_set_targets)
            if label == l
        ])

    print("computing max inertia...")
    max_clusters -= 1
    min_clusters -= 1
    max_inertia = 0
    for l in range(10):
        kmeans = KMeans(n_clusters = max_clusters, verbose = 0, n_init = 1)
        kmeans.fit(classified[l])
        print(l, "{:>18,}".format(int(kmeans.inertia_)))
        if kmeans.inertia_ > max_inertia:
            max_inertia = kmeans.inertia_

    centers = numpy.array([])
    betas = []
    for l in range(10):
        print("investigating:", l)
        lower_b = min_clusters 
        upper_b = lower_b+1
        kmeans = KMeans(n_clusters = upper_b, verbose = 0, n_init = 1)
        kmeans.fit(classified[l])
        inertia = kmeans.inertia_
        while inertia > max_inertia:
            print(lower_b, upper_b, "   \r", end = "")      
            lower_b = upper_b
            upper_b *= 2
            kmeans = KMeans(n_clusters = upper_b, verbose = 0, n_init = 1)
            kmeans.fit(classified[l])
            inertia = kmeans.inertia_

        while upper_b-lower_b > 1:
            print(lower_b, upper_b, "   \r", end = "")
            m = (upper_b+lower_b)//2
            kmeans = KMeans(n_clusters = m, verbose = 0, n_init = 1)
            kmeans.fit(classified[l])
            inertia = kmeans.inertia_
            if inertia > max_inertia:
                lower_b = m
            elif inertia < max_inertia:
                upper_b = m
            else:
                upper_b = m
                break
        clusters = upper_b
        print("number of clusters:", clusters)

        #print("computing centers...")
        kmeans = KMeans(n_clusters = clusters, verbose = 0, n_init = 3)
        pred = kmeans.fit_predict(classified[l])
        centers = numpy.append(centers, kmeans.cluster_centers_)
        print("inertia: {:>18,}".format(int(kmeans.inertia_)))
        #print("computing betas...")
        for c in range(clusters):
            cluster = numpy.array([
                i
                for idx, i in enumerate(classified[l])
                if pred[idx] == c
            ])
            cluster_sub = numpy.subtract(cluster, kmeans.cluster_centers_[c])
            norms = linalg.norm(cluster_sub, axis = 1)
            sigma = numpy.mean(norms)
            betas.append(1/(2*sigma**2))

    betas = torch.Tensor(betas)
    centers = torch.Tensor(centers).reshape((-1, 28*28))
    return centers, betas

def kmeans_unsep_features(dataset, clusters = 1000):
    train_set_array = dataset.data.numpy().reshape((-1, 28*28))

    print("computing centers...")
    kmeans = KMeans(n_clusters = clusters, verbose = 1, n_init = 3)
    pred = kmeans.fit_predict(train_set_array)
    centers = kmeans.cluster_centers_
    print("inertia: {:>18,}".format(int(kmeans.inertia_)))
    print("computing betas...")
    betas = []
    for c in range(clusters):
        print("{}/{}\r".format(c, clusters), end = "")
        cluster = numpy.array([
            i
            for idx, i in enumerate(train_set_array)
            if pred[idx] == c
        ])
        cluster_sub = numpy.subtract(cluster, kmeans.cluster_centers_[c])
        norms = linalg.norm(cluster_sub, axis = 1)
        sigma = numpy.mean(norms)
        betas.append(1/(2*sigma**2))

    betas = torch.Tensor(betas)
    centers = torch.Tensor(centers).reshape((-1, 28*28))
    return centers, betas
