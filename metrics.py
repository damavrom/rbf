import torch
from scipy.spatial.distance import mahalanobis

def distance(a, b, mode = "euclidean"):
    if mode == "manhattan":
        return manhattan(a, b)
    elif mode == "minkowski":
        return minkowski(a, b)
    elif mode == "custom":
        return custom(a, b)
    return euclidean(a, b)

def euclidean(a, b):
    d = a.add(-b).pow(2)
    if d.dim() == 1:
        d = d.sum()
    else:
        d = d.sum(axis = 1)
    d = d.sqrt()
    return d

def manhattan(a, b):
    d = a.add(-b).abs()
    if d.dim() == 1:
        d = d.sum()
    else:
        d = d.sum(axis = 1)
    return d

def minkowski(a, b):
    d = a.add(-b).pow(4)
    if d.dim() == 1:
        d = d.sum()
    else:
        d = d.sum(axis = 1)
    d = d.sqrt()
    d = d.sqrt()
    return d

def custom(a, b):
    d = a.add(-b).pow(2)
    if d.dim() == 1:
        d = d.sum()
    else:
        d = d.sum(axis = 1)
    return d
