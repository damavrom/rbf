import torch
from torchvision import datasets, transforms
import torch.optim as optim

import models
import rbf_utils

hidden_p = 2**8
learning_rate = .5
bt = 2**1
epochs = 30

trans = transforms.Compose([
    transforms.ToTensor(),
])

train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('./data', train = True, download = True, transform = trans),
    batch_size = bt,
    shuffle = True
)
test_loader = torch.utils.data.DataLoader(
    datasets.MNIST('./data', train = False, download = True, transform = trans),
    batch_size = 2**5,
    shuffle = True
)

model = models.MLP(hidden_p)
optimizer = optim.SGD(model.parameters(), lr = learning_rate, momentum = 0.5)

max_ac = 0
for epoch in range(1, epochs+1):
    models.train(model, train_loader, optimizer)
    print(epoch, end = " ")
    acuracy = models.test(model, test_loader)
    if acuracy > max_ac:
        max_ac = acuracy

print("\n### training ###")
print("hidden neurons:", hidden_p)
print("learning rate", learning_rate)
print("batch size:", bt)
print("epochs:", epochs)

print("\n### result ###")
print("best acuracy:", max_ac)
