import torch
from torchvision import datasets, transforms
import torch.optim as optim

import models
import rbf_utils

clusters = 28*28

hidden_p = 2**8
learning_rate = .5
bt = 2**1
epochs = 30

train_set = datasets.MNIST('./data', train = True, download = True)

centers, betas = rbf_utils.kmeans_unsep_features(
    train_set,
    clusters = clusters
)

print("total rbf neurons:", len(centers))

model = models.RBFN(centers, betas, hidden_p)
train_set = datasets.MNIST('./data', train = True, download = True)
test_set = datasets.MNIST('./data', train = False, download = True)

train_loader = torch.utils.data.DataLoader(
    list(zip(model.rbf(train_set.data), train_set.targets)),
    batch_size = bt,
    shuffle = True
)
test_loader = torch.utils.data.DataLoader(
    list(zip(model.rbf(test_set.data), test_set.targets)),
    batch_size = 2**5,
    shuffle = True
)

optimizer = optim.SGD(model.parameters(), lr = learning_rate, momentum = 0.5)

max_ac = 0
for epoch in range(1, epochs+1):
    models.train(model, train_loader, optimizer)
    print(epoch, end = " ")
    acuracy = models.test(model, test_loader)
    if acuracy > max_ac:
        max_ac = acuracy

print("\n### phase 1 of training ###")
print("clusters:", clusters)

print("\n### phase 2 of training ###")
print("input neurons:", len(centers))
print("hidden neurons:", hidden_p)
print("learning rate", learning_rate)
print("batch size:", bt)
print("epochs:", epochs)

print("\n### result ###")
print("best acuracy:", max_ac)
